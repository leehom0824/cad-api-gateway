package resolvers

// THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

import (
	"context"

	"gitlab.com/mayunmeiyouming/cad-api-gateway/internal/generated"

	"gitlab.com/mayunmeiyouming/cad-api-gateway/internal/models"
)

//Resolver ...
type Resolver struct{}

func (r *mutationResolver) Healthcheck(ctx context.Context) (string, error) {
	panic("not implemented")
}

func (r *mutationResolver) CreateTodo(ctx context.Context, input models.NewTodo) (*models.Todo, error) {
	todo := &models.Todo{
		Text: input.Text,
		User: &models.User{
			ID: input.UserID,
		},
	}
	return todo, nil
	// panic("not implemented")
}

func (r *queryResolver) Healthcheck(ctx context.Context) (string, error) {
	panic("not implemented")
}

func (r *queryResolver) Todos(ctx context.Context) ([]*models.Todo, error) {
	// []*models.Todo

	panic("not implemented")
}

// Mutation returns MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
